#!/usr/bin/env python
#coding:utf-8

import cv2
import numpy as np

img_rgb = cv2.imread('../data/waldo_full.jpg')
img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

template = cv2.imread('../data/waldo_feature.png', 0)
w, h = template.shape[::-1]

# Match template
res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
threshold = 0.85
loc = np.where(res >= threshold)

# Highlight matches
match_count = len(list(zip(*loc[::-1])))
for pt in zip(*loc[::-1]):
    cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 2)

font = cv2.FONT_HERSHEY_COMPLEX_SMALL
cv2.putText(img_rgb, 'Found waldo %d times' % match_count, (50, 30), font, 1, (0,0,0), 1, cv2.LINE_AA)
cv2.putText(img_rgb, 'Threshold %f' % threshold, (50,50), font, 1, (0,0, 0), 1, cv2.LINE_AA)
cv2.imshow('found waldo', img_rgb)

while True:
    if cv2.waitKey(1) & 0xff == ord('q'):
        break

cv2.destroyAllWindows()
