#!/usr/bin/env python
#coding:utf-8

import cv2
import numpy as np

img = cv2.imread('../data/book_dim.jpg')
grayscaled = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
retval, threshold = cv2.threshold(grayscaled, 12, 255, cv2.THRESH_BINARY)

th = cv2.adaptiveThreshold(grayscaled, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 115, 1)


cv2.imshow('original', img)
cv2.imshow('threshold', threshold)
cv2.imshow('adaptive threshold', th)

while True:
    if cv2.waitKey(1) & 0xff == ord('q'):
        break

cv2.destroyAllWindows()
