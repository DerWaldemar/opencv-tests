#!/usr/bin/env python
#coding:utf-8

import cv2
import numpy as np

img = cv2.imread('../data/watch.jpg', cv2.IMREAD_COLOR)

# Draw a spectacular line
cv2.line(img, (0,0), (150,150), (255, 255, 255), 15)

# Wow, such rectangle
cv2.rectangle(img, (15,15), (200,200), (0,0,255), 15)

# Even text!
font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img, 'DerWaldemar rocks!', (1,130), font, 1, (200,255,155), 2, cv2.LINE_AA)

cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()