#!/usr/bin/env python
#coding:utf-8

import cv2

cap = cv2.VideoCapture(1)

while True:
    _, frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    edges = cv2.Canny(frame, 100, 250)

    cv2.imshow('original', frame)
    cv2.imshow('canny', edges)

    if cv2.waitKey(1) & 0xff == ord('q'):
        break

cv2.destroyAllWindows()
cap.release()
