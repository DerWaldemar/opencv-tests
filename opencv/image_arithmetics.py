#!/usr/bin/env python
#coding:utf-8

import cv2
import numpy as np

# 500 x 250
img1 = cv2.imread('../data/3d_matplotlib.png')
img2 = cv2.imread('../data/2d_plot.png')
img3 = cv2.imread('../data/python.png')

# Logo on top left corner. Create a ROI
rows, cols, channels = img3.shape
roi = img1[0:rows, 0:cols]

# Create inverted mask of logo
img3gray = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)
ret, mask = cv2.threshold(img3gray, 220, 255, cv2.THRESH_BINARY_INV)
mask_inv = cv2.bitwise_not(mask)

# Black out ROI area
img1_bg = cv2.bitwise_and(roi, roi, mask = mask_inv)

# Take only region of logo from logo image
img3_fg = cv2.bitwise_and(img3, img3, mask = mask)

dst = cv2.add(img1_bg, img3_fg)
img1[0:rows, 0:cols] = dst

cv2.imshow('res', img1)

# Add images
#add = img1 + img2
#add = cv2.add(img1, img2)
#weighted = cv2.addWeighted(img1, 0.6, img2, 0.4, 0)
#cv2.imshow('image', add)
#cv2.imshow('image', weighted)


cv2.waitKey(0)
cv2.destroyAllWindows()
